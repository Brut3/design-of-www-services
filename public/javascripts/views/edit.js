define([
  'jquery', 
  'common',
  'form'
], function($, Common, Form) {
  
  var initialize = function() {
    Form.initialize();
  };
  
  return {
    initialize : initialize
  };
});
