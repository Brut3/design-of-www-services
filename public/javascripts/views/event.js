/*
 * Functionality for event view.
 */
define([
    'jquery',
    'common',
    'text!/../resources/templates/user_item',
    'text!resources/checked-item.html'
], function($, Common, UserItem, CheckedItem) {

  var noUserItemsText = $('#userItems .emptiness-indicator');
  var noNeededItemsText = $('#eventItemPool .emptiness-indicator');

  var selectedNeededItems = {};

  var initialize = function() {
    noUserItemsText.show();
    noNeededItemsText.hide();
    noNeededItemsText.removeClass('hidden');
    if ($('#eventItemPool .needed-item[class~="visible"]').size() < 1) {
      noNeededItemsText.show();
    }

    // Bind events
    $('#eventItemPool .needed-item').click(neededItemSelected);
    $('#userItemConfirm').click(function(event) {
      userItemsConfirmed();
    });
  };

  /*
   * Called when an item from the needed items pool is clicked.
   * Hides the selected item and adds it to user items
   */
  var neededItemSelected = function(event) {
    var item = $(event.target);
    // If target is not a div (span clicked), search the correct div.
    if (item.not('div .needed-item')) {
      item = item.closest('div .needed-item');
    }
    if (noUserItemsText.size() > 0) {
      noUserItemsText.hide();
    }
    hideNeededItem(item);
    addItemToUserPool(item);
    if ($('#eventItemPool .needed-item[class~="visible"]').size() < 1) {
      noNeededItemsText.show();
    }
    selectedNeededItems[itemName(item)] = item;
  };

  var itemName = function(item) {
    return item.find('.name').first().text();
  };

  var hideNeededItem = function(item) {
    item.hide();
    item.removeClass('visible');
  };

  var showNeededItem = function(item) {
    item.show();
    item.addClass('visible');
    noNeededItemsText.hide();
  };

  /*
   * Adds a selected item to user items pool and updates the 
   * event bindings for that item.
   */
  var addItemToUserPool = function(item) {
    noUserItemsText.hide();
    var addedItem = $('#userItems').prepend(UserItem).children().first();
    $('#eventUserActionsForm').height($('#userItems').outerHeight());
    addedItem.hide();
    addedItem.find('.item-id').text(item.find('.item-id').text());
    addedItem.find('.name').text(item.find('.name').text());
    addedItem.find('.amount').attr('max', item.find('.needed-quantity').text());
    addedItem.slideDown('fast', function(){});
    addedItem.find('.remove-item').click(removeUserItemFromPool);
    Common.updateBindings();
  };

  var removeUserItemFromPool = function(event) {
    var item = $(event.target);
    if (item.not('div .user-item')) {
      item = item.closest('div .user-item');
    }
    showNeededItem(selectedNeededItems[itemName(item)]);
    item.slideUp('fast', function() {
      item.remove();
      if ($('#userItems .user-item').size() < 1) {
        noUserItemsText.slideDown('fast', function(){});
      }
    });
    $('#eventUserActionsForm').height($('#userItems').outerHeight());
  };

  var userItemsConfirmed = function() {
    // Gather input
    var userName = $('#userName').val();
    var userItems = $('#userItems .user-item');

    // Don't continue unless valid
    if(!validateUserName(userName)) {
      return;
    }
    // Check if the first new row should be even or odd
    var oddRow = "row-odd";
    var evenRow = "row-even";
    var rowType = 0;
    var lastAddedItem = $('#checkedEventItemsPool tr').last();
    if (lastAddedItem.size() > 0) {
      rowType = lastAddedItem.hasClass(evenRow) ? 0:1;
    }

    var itemsData = {};
    // For each user item
    $.each(userItems, function(i, elem) {
      elem = $(elem);
      // Create a new jQuery object from the 'CheckedItem' template
      var newItem = $(CheckedItem);
      if (rowType % 2 !== 0) {
        newItem.removeClass(oddRow);
        newItem.addClass(evenRow);
      } else {
        newItem.removeClass(evenRow);
        newItem.addClass(oddRow);
      }
      rowType++;
      // Values
      // FIXME Template hack
      var itemData = {
          "item-id": elem.find('.item-id').text(),
          "item-name": elem.find('.name').text(),
          "item-amount": elem.find('.amount').val(),
          "item-user": userName,
          "item-comment": elem.find('.comment').val()
      };

      itemsData['item'+(i+1)] = itemData;

      newItem.find('.item-name span').text(itemData['item-name']);
      newItem.find('.item-amount span').text(itemData['item-amount']);
      newItem.find('.item-author span').text(itemData['item-user']);
      newItem.find('.item-comment span').text(itemData['item-comment']);
      $('#checkedEventItemsPool').append(newItem);
    });

    var eventId = $('#eventid').val();

    var attendantData = {
        "id": eventId,
        "name": userName,
        "items": itemsData
    };

    // POST attendant data to server without reloading the page
    postAddAttendant(attendantData);

    $('#userName').val('');
    $.each($('#userItems .user-item'), function(i, elem) {
      $(elem).remove();
    });
    noUserItemsText.slideDown('fast', function(){});
  };

  var validateUserName = function(name) {
    if (name === '') {
      alert('Name cannot be empty!');
      return false;
    }
    return true;
  };

  var postAddAttendant = function(attendantData) {
    $.ajax({
      type: "POST",
      url: "/events/add-attendant",
      data: attendantData,
      dataType: "json"
    });
  };

  return {
    initialize: initialize
  }
});