/*
 * Functionality for the index page.
 */
define([
  'jquery', 
  'common',
  'form'
], function($, Common, Form) {
  
  var form = $('#eventCreationForm');
  var formHeight;

  var initialize = function() {
    form.removeClass('hidden');
    formHeight = form.show().height();
    form.hide();
    form.height(formHeight);
    Form.initialize();
    bindEvents();
  };
  var bindEvents = function() {
    $('#createAnEvent').click(function(event) {
      toggleEventCreationForm();
    });
  };
  /*
   * Show or hide the event creation form with an animation.
   */
  var toggleEventCreationForm = function() {
    form = $('#eventCreationForm');
    if(form.hasClass('visible')) {
      formHeight = form.height();
      form.removeClass('visible');
      form.slideUp('slow', function() {
        form.height(formHeight);
      });
      $('#createEventLabel').text('Create an event');
    } else {
      form.slideDown('slow', function() {
        form.css('height','');
      });
      form.addClass('visible');
      $('#createEventLabel').text('Cancel');
    }
  };
  return {
    initialize : initialize
  };
});
