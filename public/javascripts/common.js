/*
 * Runs utility procedures common for many situations.
 */
define([ 
  'jquery',
  'text!resources/about.html',
  'text!resources/privacy.html',
  'text!resources/feedback.html'
], function($, AboutText, PrivacyText, FeedbackText) {
  
  var menuDialog = $('#dialog');

  /*
   * Executes module-related functionality and is exposed outside the module
   */
  var initialize = function() {
    doBindings();
    removeStaticElements();
  };

  /*
   * Searches for all elements, that should have a hover functionality
   * associated with a certain style. (jQuery-ui)
   */
  var doBindings = function() {
    // Bind jQuery-ui hover actions
    $('.ui-state-default').hover(function() {
      $(this).addClass('ui-state-hover');
    }, function() {
      $(this).removeClass('ui-state-hover');
    });
    
    // Bind hint element hover actions.
    var hintElements = $('.hint');
    $.each(hintElements, function(i, elem) {
      elem = $(elem); // jQuery obj
      var hintMsg = elem.find('.hint-message');
      hintMsg.hide();
      elem.find('.hint-button').hover(function() {
        hintMsg.show();
      }, function() {
        hintMsg.hide();
      });
    });
    
    // Bind information links
    $('#about').click(openAboutDialog);
    $('#privacy').click(openPrivacyDialog);
    $('#feedback').click(openFeedbackDialog);
  };

  /*
   * Removes all elements (e.g. links) that aren't needed when js available.
   * These elements are marked with the 'no-js-element' class. This way, the
   * functionality is progressively enhanced when js is available.
   */
  var removeStaticElements = function() {
    var staticElements = $('.no-js-element');
    $.each(staticElements, function(i, elem) {
      elem = $(elem);
      var content = elem.contents();
      elem.replaceWith(content);
    });
  };
  
  var openAboutDialog = function() {
    openDialog('About', AboutText);
  };
  
  var openPrivacyDialog = function() {
    openDialog('Privacy', PrivacyText);
  };
  
  var openFeedbackDialog = function() {
    openDialog('Feedback', FeedbackText);
  };
  
  var openDialog = function(title,html) {
    menuDialog.html(html);
    menuDialog.dialog({
      title: title, 
      height: 500, width: 740, 
      position: 'center',
      show: 'fade',
      modal: 'true',
      buttons: {'Close': function() {menuDialog.dialog('close')}}
      });
  };

  return {
    initialize : initialize,
    updateBindings : doBindings
  }
});
