define([
  'jquery', 
  'common',
  'text!resources/item-field.html'
], function($, Common, ItemFieldTemplate) {

  var addedItemsGroup = $('#itemList .inputgroup');
  var itemFieldCount = $('#itemList .item-field').size();
  var itemFieldIndex = itemFieldCount;

  var initialize = function() {
    bindEvents();
  };
  var bindEvents = function() {
    $('#addNewItemButton').click(function(event) {
      addNewItemToList();
    });
    $('#eventCreationForm .remove-item').click(removeItemFromList);
  };
  var addNewItemToList = function() {
    // TODO Dirty template hack
    var newItemField = $(ItemFieldTemplate.replace(/XXX/g, itemFieldIndex++));
    addedItemsGroup.append(newItemField);
    newItemField.find('.remove-item').click(removeItemFromList);
    $('#itemList').height(addedItemsGroup.height());
    itemFieldCount++;
    Common.updateBindings();
  };
  var removeItemFromList = function(event) {
    if(itemFieldCount > 1) {
      var field = $(event.target).closest('div .item-field');
      field.remove();
      itemFieldCount = $('#itemList .item-field').size();
      $('#itemList').height(addedItemsGroup.height());
    } else {
      alert('You must have at least one item.')
    }
  }
  return {
    initialize : initialize
  };
});
