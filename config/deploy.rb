require 'bundler/capistrano'
set :application, "Camillo"
set :repository,  "git@bitbucket.org:Brut3/design-of-www-services.git"

# If you aren't deploying to /u/apps/#{application} on the target
# servers (which is the default), you can specify the actual location
# via the :deploy_to variable:
set :scm, :git
set :scm_username, "Brut3"
set :branch, 'master'
set :user, 'jaraunio'
set :scm_verbose, true
set :deploy_to, "/home/2012/groups/hype2011/demo/#{application}"
set :use_sudo, false
#set :deploy_via, :export

role :app, "demo.hype2011.hype.tml.hut.fi"
role :web, "demo.hype2011.hype.tml.hut.fi"
role :db,  "demo.hype2011.hype.tml.hut.fi", :primary => true

ssh_options[:forward_agent] = true

before "deploy:cold", 
    "deploy:install_bundler"

task :install_bundler, :roles => :app do
    run "type -P bundle &>/dev/null || { gem install bundler --no-rdoc --no-ri; }"
end

#set :default_environment, {
#    'PATH' => "/usr/local/bin:/bin:/usr/bin:/bin:/var/lib/gems/1.8/bin",
#    'GEM_HOME' => 'var/lib/gems/1.8/gems',
#    'GEM_PATH' => 'var/lib/gems/1.8/gems',
#    'BUNDLE_PATH' => 'var/lib/gems/1.8/gems'  
#}


namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "touch #{File.join(current_path, 'tmp', 'restart.txt')}"
  end
end
