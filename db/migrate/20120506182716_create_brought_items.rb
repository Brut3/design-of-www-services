class CreateBroughtItems < ActiveRecord::Migration
  def self.up
    create_table :brought_items do |t|
      t.integer :quantity
      t.string :comment
      t.integer :attendant_id
      t.integer :item_id

      t.timestamps
    end
  end

  def self.down
    drop_table :brought_items
  end
end
