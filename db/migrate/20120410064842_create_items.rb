class CreateItems < ActiveRecord::Migration
  def self.up
    create_table :items do |t|
      t.string :name
      t.integer :needed_quantity
      t.integer :current_quantity, :default => '0'
      t.integer :event_id
      t.timestamps
    end
  end

  def self.down
    drop_table :items
  end
end
