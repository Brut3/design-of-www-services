class CreateAttendants < ActiveRecord::Migration
  def self.up
    create_table :attendants do |t|
      t.string :name
      t.integer :event_id

      t.timestamps
    end
  end

  def self.down
    drop_table :attendants
  end
end
