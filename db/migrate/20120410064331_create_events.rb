class CreateEvents < ActiveRecord::Migration
  def self.up
    create_table :events do |t|
      t.string :name
      t.string :description
      t.string :email
      t.integer :hash_val
      t.boolean :own_items_allowed
      t.timestamps
    end
  end

  def self.down
    drop_table :events
  end
end
