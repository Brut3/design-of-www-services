class EventsController < ApplicationController

  # GET /events
  # def index
  #  @events = Event.all
  # end

  # GET /events/1
  def show
    @event = Event.find(params[:id])
    @items = @event.items
    @title = @event.name
    @main_app = '/resources/js/main/event'
  end
  
  # For a non-javascript version of the show view.
  def show_simple
    @event = Event.find(params[:id])
    @items = @event.items
    @title = @event.name
    render :action => "show"
  end
  
  def show_urls
    @event = Event.find(params[:id])
  end
  
  # GET /events/new
  def new
    @item_count = params[:item_count]?params[:item_count]:"1"
    @item_count = Integer(@item_count)
    @js = false
    @event = Event.new
    @title = "New event"
    for i in 1..@item_count
      @event.items.build
    end
  end
  
  # Helper method for adding a new item
  def add_item    
    @event.items.new
    render :action => "new"
  end
  
  def mainjs
    @app = params[:app]
    respond_to do |format|
      format.js
    end
  end
  
  # GET /
  def home
    @title = "Home"
    @js = true
    @event = Event.new
    @main_app = '/resources/js/main/home'
    @event.items.build
  end
  
  # GET /events/1312314/54325425
  def edit
    @event = Event.where("hash_val = ?", params[:hash]).first
    @main_app = '/resources/js/main/edit'
    @title = "Editing " + @event.name
  end

  
  # POST /events
  def create
    @event = Event.new(params[:event])
    
      if @event.save
        redirect_to(@event, :notice => "Notice")
      else
        @main_app = '/resources/js/main/home'
        @title = "Home"
        render :action => "home"
      end
  end

  # PUT /events/1
  def update
    @event = Event.find(params[:id])

      if @event.update_attributes(params[:event])
        redirect_to(@event, :notice => 'Event was successfully updated.')
      else
        render :action => "edit"
      end
  end

  # DELETE /events/1
  def destroy
    @event = Event.find(params[:id])
    @event.destroy
    redirect_to(events_url)
  end
  
  # Adds a new attendant to event.
  def add_attendant
    @event = Event.find(params[:id])
    # If attendant already exists, items are added to him/her.
    if @event.attendants.exists?(:name => params[:name])
      @attendant = @event.attendants.where("name = ?", params[:name]).first
    else
      @attendant = @event.attendants.build(:name => params[:name])
    end
    # Process all chosen items.
    params[:items].each do |item|
      item = item[1]
      if (item['item-amount'] == "0")
        next
      end
      @bi = @attendant.brought_items.build(:item_id => item['item-id'], :quantity => item['item-amount'], :comment => item['item-comment'])
      @bi.item.current_quantity += @bi.quantity
      @bi.item.save
    end
    @attendant.save
    respond_to do |format|
      format.json { render :json => @event.brought_items }
      format.html { redirect_to @event }
    end
  end
  
  # For retrieving HTML templates for javascript
  def template
    @template_name = "_"+params[:tmplname]
    respond_to do |format|
      format.html { render @template_name, :layout => false }
    end
  end

end
