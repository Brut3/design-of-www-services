# == Schema Information
#
# Table name: attendants
#
#  id         :integer         not null, primary key
#  name       :string(255)
#  event_id   :integer
#  created_at :datetime
#  updated_at :datetime
#

class Attendant < ActiveRecord::Base
  belongs_to :event
  has_many :brought_items, :dependent => :destroy
  attr_accessible :name
  validates :name,  :presence => true,
                    :length => { :maximum => 50 }
  validates_associated :brought_items                 
end


