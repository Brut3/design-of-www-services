# == Schema Information
#
# Table name: events
#
#  id                :integer         not null, primary key
#  name              :string(255)
#  description       :string(255)
#  email             :string(255)
#  hash_val          :string(255)     default("0")
#  own_items_allowed :boolean
#  created_at        :datetime
#  updated_at        :datetime
#

class Event < ActiveRecord::Base
  before_create :set_id, :set_hash
  has_many :items, :dependent => :destroy
  has_many :attendants, :dependent => :destroy
  has_many :brought_items, :through => :attendants
  attr_accessible :id, :name, :description, :email, :items, :items_attributes, :hash_val, :own_items_allowed
  accepts_nested_attributes_for :items, :allow_destroy => true
  
  email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  
  validates :name,  :presence => { :message => "The event should have a name." },
                    :length => { :maximum => 50, :message => "The name of the event should be less than 50 characters."}
  validates :email, :presence => { :message => "The e-mail adress is required." },
                    :format   => { :with => email_regex, :message => "The e-mail adress should be form example@mail.com." },
                    :length => { :maximum => 255 }
  validates :description, :length => { :maximum => 255, :message => "The length of the description should be less than 255 characters." }
                              
  validates_associated :items, :attendants, :brought_items                  
  private 
  
  #Sets 8-digit random integer for primary key
  def set_id
    self.id = Array.new(8){rand(9)}.join
  end
  
  def set_hash
    self.hash_val = Array.new(8){rand(9)}.join
  end
  
end



