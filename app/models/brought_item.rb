# == Schema Information
#
# Table name: brought_items
#
#  id           :integer         not null, primary key
#  quantity     :integer
#  comment      :string(255)
#  attendant_id :integer
#  item_id      :integer
#  created_at   :datetime
#  updated_at   :datetime
#

class BroughtItem < ActiveRecord::Base
  #TODO Add before callback to check the quantity
  belongs_to :attendant
  belongs_to :item
  attr_accessible :comment, :quantity, :item_id
  validates :comment, :length => { :maximum => 255 }
  validates :quantity, :presence => true,
                       :numericality => { :only_integer => true, :greater_than => 0 }
  validate :quantity_cannot_be_greater_than_current_quantity                   
  
  def quantity_cannot_be_greater_than_current_quantity
    item = Item.find(self.item_id)
    if self.quantity > item.needed_quantity
      errors.add(:quantity, "Quantity can't be greater than the required amount.")
    end
  end  
                      
end



