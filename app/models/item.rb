# == Schema Information
#
# Table name: items
#
#  id               :integer         not null, primary key
#  name             :string(255)
#  needed_quantity  :integer
#  current_quantity :integer         default(0)
#  event_id         :integer
#  created_at       :datetime
#  updated_at       :datetime
#

class Item < ActiveRecord::Base
  belongs_to :event
  has_many :brought_items
  attr_accessor :item_id
  attr_accessible :name, :needed_quantity, :current_quantity
  
  validates :name,  :presence => { :message => "The item should have a name." },
                    :length => { :maximum => 50, :message => "The name of the item should be less than 50 characters." }
  validates :needed_quantity, :presence => true,
                              :numericality => { :only_integer => true, :greater_than => 0, 
                                                  :message => "Minimum quantity for items is 1." }                                                   
end



